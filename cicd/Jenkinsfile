def K8S_TEMPLATE = """
apiVersion: v1
kind: Pod
metadata:
  annotations:
    vault.hashicorp.com/agent-init-first: 'true'
    vault.hashicorp.com/agent-inject: 'true'
    vault.hashicorp.com/agent-pre-populate-only: 'true'
    vault.hashicorp.com/role: jenkins
    vault.hashicorp.com/agent-inject-secret-config.json: kv/jenkins/harbor
    vault.hashicorp.com/agent-inject-template-config.json: |
      {{- with secret "kv/jenkins/harbor" -}}
      {
        "auths": {
          "harbor.prd.k3s.vm": {
            
            "auth": "{{ .Data.auth }}"
          }
        }
      }
      {{- end }}
    vault.hashicorp.com/agent-inject-secret-slack.env: kv/jenkins/slack
    vault.hashicorp.com/agent-inject-template-slack.env: |
      {{- with secret "kv/jenkins/slack" -}}
      _SLACK_URI={{ .Data.SLACK_WEBHOOK_OCI_TEMPLATES }}
      {{- end }}
spec:
  serviceAccountName: jenkins
  containers:
  - name: docker
    image: harbor.prd.k3s.vm/library/oci-builder:latest
    command:
    - sh
    env:
    - name: DOCKER_HOST
      value: "tcp://ws-dev-0.fwc.dc.dev.apps.vm:2375"
    tty: true
"""

pipeline {
    agent { kubernetes yaml: "${K8S_TEMPLATE}" }
    environment {
        _SLACK_ENV = "/vault/secrets/slack.env"
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '5'))
    }
    stages {
        stage('PREREQ: prepare build environment') {
            steps {
                container('docker') {
                    sh 'mkdir -p $HOME/.docker'
                    sh 'cp /vault/secrets/config.json $HOME/.docker/'
                }
            }
        }
        stage('OCI: lint') {
            steps {
                container('docker') {
                    sh 'make oci-lint'
                }
            }
        }
        stage('OCI: build') {
            steps {
                container('docker') {
                    sh 'make oci-build'
                    // TODO: Update logic for tagging to latest
                    sh 'make OCI_TAG=latest oci-build'
                }
            }
        }
        stage('OCI: publish') {
            steps {
                container('docker') {
                    sh 'make oci-publish'
                    sh 'make OCI_TAG=latest oci-publish'
                }
            }
        }
        stage('OCI: clean') {
            steps {
                container('docker') {
                    sh 'make oci-clean'
                    sh 'make OCI_TAG=latest oci-clean'
                }
            }
        }
    }
    post {
        always {
            container('docker') {
                script {
                    sh """
                    printenv
                    make _PAYLOAD_ENV_FILE=${_SLACK_ENV} \
                    CI_NAME="${env.JOB_BASE_NAME}" \
                    CI_URL=${env.BUILD_URL} \
                    CI_STATUS=${currentBuild.currentResult} \
                    GIT_URL=${env.GIT_URL} \
                    GIT_BRANCH=${env.GIT_BRANCH} \
                    GIT_COMMIT=${env.GIT_COMMIT} slack-ci
                    """
                }
            }
        }
    }
    triggers {
        cron 'H 21 * * *'
    }
}